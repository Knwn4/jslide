isInstalled = [];
isNotInstalled = [];
isHtml = false;

let is = [];
let br = [];
let ext = [];
is["Chrome"] = is["IE"] = is["Edge"] = is["Firefox"] = is["Opera"] = is[
  "Safari"
] = is["Brave"] = is["Tor"] = is["New Edge"] = false;
br["Chrome"] = br["IE"] = br["Edge"] = br["Firefox"] = br["Opera"] = br[
  "Safari"
] = br["Brave"] = br["Tor"] = br["New Edge"] = 0;
ext["Chrome"] = ext["IE"] = ext["Edge"] = ext["Firefox"] = ext["Opera"] = ext[
  "Safari"
] = ext["Brave"] = ext["Tor"] = ext["New Edge"] = 0;

extList = [
  {
    name: "Avast Online Security",
    id: "gomekmidlodglbbmalcneegieacbdmki",
    object: "common/ui/icons/icon128.png",
    browser: "Chrome",
  },

  {
    name: "Avast SafePrice | Comparison, deals, coupons",
    id: "eofcbnmajmjmplflapaojjnihcjkigck",
    object: "common/ui/icons/logo-safe-price-32.png",
    browser: "Chrome",
  },

  {
    name: "AVG Online Security",
    id: "nbmoafcmbajniiapeidgficgifbfmjfo",
    object: "common/ui/icons/icon128.png",
    browser: "Chrome",
  },

  {
    name: "Avira Password Manager",
    id: "caljgklbbfbcjjanaijlacgncafpegll",
    object: "images/icons/16x16.png",
    browser: "Chrome",
  },

  {
    name: "Grammarly for Chrome",
    id: "kbfnbcaeplbcioakkpcpgfkobkghlhen",
    object: "src/css/Grammarly.styles.css",
    browser: "Chrome",
  },

  {
    name: "GIPHY for Chrome",
    id: "jlleokkdhkflpmghiioglgmnminbekdi",
    object: "img/giphy_icon_19.png",
    browser: "Chrome",
  },

  {
    name: "GNOME Shell integration",
    id: "gphhapmejobijbbhgpjhcjognlahblep",
    object: "include/sweettooth-api.js",
    browser: "Chrome",
  },

  {
    name: "Google Hangouts",
    id: "nckgahadagoaajjgafhacjanaoiihapd",
    object: "images_5/icon_48.png",
    browser: "Chrome",
  },

  {
    name: "Honey",
    id: "bmnlcjabgnpnenekpadlanbbkooimhnj",
    object: "honey_pay/index.html",
    browser: "Chrome",
  },

  {
    name: "Keeper® Password Manager & Digital",
    id: "bfogiafebfohielmmehodmfbbebbbpei",
    object: "images/logo.png",
    browser: "Chrome",
  },

  {
    name: "LastPass: Free Password Manager",
    id: "hdokiejnpimakedhajhdlcegeplioahd",
    object: "images/icon48.png",
    browser: "Chrome",
  },

  {
    name: "McAfee Endpoint Security For Mac Web Control",
    id: "kaedchgajfpgipflgbgeeiiajekblklm",
    object: "Resources/mcafee.gif",
    browser: "Chrome",
  },

  {
    name: "McAfee WebAdvisor For Mac",
    id: "cdchjnfonmcmbclbcbcicknddglglnha",
    object: "Resources/mcafee.gif",
    browser: "Chrome",
  },

  {
    name: "McAfee® Web Boost",
    id: "klekeajafkkpokaofllcadenjdckhinm",
    object: "images/logo.png",
    browser: "Chrome",
  },

  {
    name: "Microsoft S/MIME",
    id: "maafgiompdekodanheihhgilkjchcakm",
    object: "Options.html",
    browser: "Chrome",
  },

  {
    name: "Norton Safe Search",
    id: "eoigllimhcllmhedfbmahegmoakcdakd",
    object: "content/images/norton-icon-128.png",
    browser: "Chrome",
  },

  {
    name: "True Key™ by McAfee",
    id: "cpaibbcbodhimfnjnakiidgbpiehfgci",
    object: "pages/tk-inpages-frame.html",
    browser: "Chrome",
  },

  {
    name: "Zoom Scheduler",
    id: "kgjfgplpablkjnlkjmjdecgdpfankdle",
    object: "images/icon.svg",
    browser: "Chrome",
  },

  {
    name: "LastPass",
    id: "hnjalnkldgigidggphhmacmimbdlafdo",
    object: "images/LP-Logo.png",
    browser: "Opera",
  },

  {
    name: "LastPass",
    id:
      "hdokiejnpimakedhajhdlcegeplioahd_LastPassLastPassFreePasswordManager_qq0fmhteeht3j",
    object: "images/icon48.png",
    browser: "Edge",
  },

  {
    name: "LastPass (NEW EDGE)",
    id: "bbcinlkgjjkejfdpemiealijmmooekmp",
    object: "images/icon48.png",
    browser: "New Edge",
  },

  {
    name: "AdBlocker Ultimate (Linux)",
    id: "11cbe11a-7b24-4c3d-bcf5-92689ccb345e",
    object: "pages/shield/font.css",
    browser: "Firefox",
  },

  {
    name: "Ghostery – Privacy Ad Blocker (Linux)",
    id: "d4a197bb-0a0f-4f1f-8ecc-c892ffbeacb1",
    object: "app/images/icon16.png",
    browser: "Firefox",
  },

  {
    name: "HTTPS Everywhere (Linux)",
    id: "9d5e54ad-ae51-402b-888e-5913e83133cc",
    object: "pages/cancel/index.html",
    browser: "Firefox",
  },

  {
    name: "LastPass (Linux)",
    id: "2704a8f1-e18a-4dae-ae6b-8f219c1ee3cb",
    object: "images/LP-Logo.png",
    browser: "Firefox",
  },

  {
    name: "uBlock Origin (Linux)",
    id: "6df2068c-47e6-4833-a43d-b34c4852b462",
    object: "web_accessible_resources/noop.txt",
    browser: "Firefox",
  },

  {
    name: "AdBlocker Ultimate (Windows)",
    id: "a7b4e5d2-52bb-4464-b5cd-9ac938f380ab",
    object: "pages/shield/font.css",
    browser: "Firefox",
  },

  {
    name: "Ghostery – Privacy Ad Blocker (Windows)",
    id: "41923c07-7fb8-470a-87bb-2664d882df2d",
    object: "app/images/icon16.png",
    browser: "Firefox",
  },

  {
    name: "HTTPS Everywhere (Windows)",
    id: "523aa09b-1ba9-461d-80c9-d13b1c98ee58",
    object: "pages/cancel/index.html",
    browser: "Firefox",
  },

  {
    name: "LastPass (Windows)",
    id: "fb66bdcc-ce35-487c-bc13-b2578463e18e",
    object: "images/LP-Logo.png",
    browser: "Firefox",
  },

  {
    name: "uBlock (Windows)",
    id: "faa57af1-0d5d-4e91-b493-81c69078552c",
    object: "web_accessible_resources/noop.txt",
    browser: "Firefox",
  },

  {
    name: "HTTPS Everywhere (TOR - Windows)",
    id: "59320469-dd9d-4220-b785-a63cae5b1ca0",
    object: "pages/cancel/index.html",
    browser: "Tor",
  },
  
  {
    name: "Wappalyzer (Linux)",
    id: "gppongmhjkpfnbhagpmjfkannfbllamg",
    object: "js/content.js",
    browser: "Chrome",
  },

  { name: "", id: "", object: "", browser: "" },
];

// Uses JavaScript error from an undefined object and browser specific objects to determine browser
function findBrowser() {
  isHtml = checkHtml();
  try {
    // Unique (hopefully) variable for a unique item that should not already exist.
    let qwertyu = asdfghj.length;
  } catch (e) {
    //Chrome
    try {
      -1 < e.toString().search("(asdfghj).([a-z]{2}).([a-z]{3}).([a-z]{7})") &&
        br["Chrome"]++;
      window.chrome && br["Chrome"]++; //too vague for chromium browsers
      window.chrome.csi && br["Chrome"]++;
      //window.chrome.runtime && br["Chrome"]++;
      //Deduct if Opera
      window.opr && br["Chrome"]--;
      window.hasOwnProperty("onoperadetachedviewchange") && br["Chrome"]--;
      //Deduct if Brave
      window.Brave && br["Chrome"]--;
      navigator.brave && br["Chrome"]--;
    } catch (e) {}

    //IE
    try {
      -1 < e.toString().search("'(asdfghj)'.([a-z]{2}).([a-z]{9})") &&
        br["IE"]++;
      ///*@cc_on!@*/ true && br["IE"]++;
      window.__BROWSERTOOLS_CONSOLE_SAFEFUNC && br["IE"]++;
      !!document.documentMode && br["IE"]++;
    } catch (e) {}

    //Edge
    try {
      -1 <
        e.toString().search("'(asdfghj)'.([a-z]{2}).([a-z]{3}).([a-z]{7})") &&
        br["Edge"]++;
      window.msCredentials && br["Edge"]++;
      window.MSAssertion && br["Edge"]++;
    } catch (e) {}

    //NEWWW Edge
    try {
      -1 < e.toString().search("(asdfghj).([a-z]{2}).([a-z]{3}).([a-z]{7})") &&
        br["New Edge"]++;
      navigator.xr && br["New Edge"]++ && br["New Edge"]++;
      //window.MSAssertion && br["New Edge"]++;
    } catch (e) {}

    //FF
    try {
      -1 < e.toString().search("(asdfghj).([a-z]{2}).([a-z]{3}).([a-z]{7})") &&
        br["Firefox"]++;
      window.InstallTrigger && br["Firefox"]++;
      0 <= window.mozInnerScreenX && br["Firefox"]++;
      if (br["Firefox"] == 3) {
        typeof window.onpointerup === "undefined" && br["Tor"]++;
        typeof window.onvrdisplayconnect === "undefined" && br["Tor"]++;
        !window.applicationCache && br["Tor"]++;
        if (br["Tor"] == 3) br["Firefox"] = 0;
      }
    } catch (e) {}

    //Opera
    try {
      //var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
      -1 < e.toString().search("(asdfghj).([a-z]{2}).([a-z]{3}).([a-z]{7})") &&
        br["Opera"]++;
      window.opr && br["Opera"]++;
      window.hasOwnProperty("onoperadetachedviewchange") && br["Opera"]++;
      window.Brave && br["Opera"]--;
    } catch (e) {}

    //Safari
    //https://stackoverflow.com/questions/9847580/how-to-detect-safari-chrome-ie-firefox-and-opera-browser/9851769
    // https://stackoverflow.com/questions/9847580/how-to-detect-safari-chrome-ie-firefox-and-opera-browser
    try {
      -1 <
        e
          .toString()
          .search("([A-Z])[a-z]{2}'[a-z].[a-z]{4}.[a-z]{8}: (asdfghj)") &&
        br["Safari"]++;
      window.ApplePayError && br["Safari"]++;
      window.ApplePaySetup && br["Safari"]++;
      //var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

      // !(/constructor/i.test(window.HTMLElement))&&br['Safari']++
      // !(function (p) { return p.toString() === "[object SafariRemoteNotification]"; })&&br['Safari']++
      // !(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification))&&br['Safari']++
    } catch (e) {}

    //Brave
    try {
      -1 < e.toString().search("(asdfghj).([a-z]{2}).([a-z]{3}).([a-z]{7})") &&
        br["Brave"]++;
      window.Brave && br["Brave"]++;
      navigator.brave && br["Brave"]++;
      window.opr && br["Brave"]--;
    } catch (e) {}

    let found = 0;
    let result = "";
    for (let key in is) {
      let value = br[key];
      if (value == 3) {
        var cMessage =
          "Very Likely: " + key + " - " + value + " out of 3 tests passed";
        console.log(cMessage);
        found++;
        result = key;
        is[key] = true;
        if (isHtml == true) {
          // var node = document.createElement('li');
          // var textNode = document.createTextNode(cMessage);
          // node.appendChild(textNode);
          // document.getElementById('bList').appendChild(node);
          document.getElementById("bList").innerText += cMessage;
          document.getElementById("bList").innerHTML += "<br>";
        }
      } else if (value == 2) {
        var cMessage =
          "Possibly: " + key + " - " + value + " out of 3 tests passed";
        console.log(cMessage);
        // var node = document.createElement('li');
        // var textNode = document.createTextNode(cMessage);
        // node.appendChild(textNode);
        // document.getElementById('bList').appendChild(node);
        document.getElementById("bList").innerText += cMessage;
        document.getElementById("bList").innerHTML += "<br>";
      }
      //console.log(key,value);
    }

    if (found == 2) {
      return result;
    } else {
      return "MULTIPLE RESULTS";
    }
  }
}

function checkFile(address, name, isHtml, browser) {
  var tester = new XMLHttpRequest();
  tester.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      if (this.status == 200) {
        //console.log("File is available");
        isInstalled.push(name);
        console.log("Extension: " + name);
        if (isHtml == true) {
          // var node = document.createElement('li');
          // var textNode = document.createTextNode(name);
          // node.appendChild(textNode);
          // document.getElementById('eList').appendChild(node);
          document.getElementById("eList").innerHTML +=
            "<li>" + "[" + browser + "] " + name + "</li>";
          ext[browser]++;
        }
      } else {
        console.log("The file returned status code " + this.status);
        isNotInstalled.push(name);
      }
    }
  };
  tester.open("GET", address, true);
  tester.send();
}

function checkExtensions() {
  try {
    isHtml = checkHtml();
    for (i = 0; i < extList.length; i++) {
      if (extList[i].name && extList[i].id && extList[i].object) {
        if (window.chrome) {
          if (
            (br["New Edge"] == 3 && (extList[i].browser == "New Edge" || extList[i].browser == "Chrome")) 
          ) {
            address = "chrome-extension://" + extList[i].id + "/" + extList[i].object;
            checkFile(address, extList[i].name, isHtml, extList[i].browser);
            address = "extension://" + extList[i].id + "/" + extList[i].object;
            checkFile(address, extList[i].name, isHtml, extList[i].browser);
            continue;
          } else if (br["Edge"] == 3 && extList[i].browser == "Edge"){
            address = "ms-browser-extension://" + extList[i].id + "/" + extList[i].object;
          } else {
            address = "chrome-extension://" + extList[i].id + "/" + extList[i].object;
          }
        } else if (window.InstallTrigger) {
          address =
            "moz-extension://" + extList[i].id + "/" + extList[i].object;
        }
        checkFile(address, extList[i].name, isHtml, extList[i].browser);
      } else {
        console.log("DETAILS MISSING");
      }
    }
    console.log("Installed:", isInstalled);
  } catch (e) {}
}

function checkHtml() {
  var isHtml = document.getElementById("bResults");
  if (isHtml) {
    return true;
  } else {
    return false;
  }
}

function extCount() {
  document.getElementById("extC").innerText = extList.length;
}

//https://developer.mozilla.org/en-US/docs/Web/HTTP/Browser_detection_using_the_user_agent
function touchy() {
  var hasTouchScreen = false;
  if ("maxTouchPoints" in navigator) {
    hasTouchScreen = navigator.maxTouchPoints > 0;
  } else if ("msMaxTouchPoints" in navigator) {
    hasTouchScreen = navigator.msMaxTouchPoints > 0;
  } else {
    var mQ = window.matchMedia && matchMedia("(pointer:coarse)");
    if (mQ && mQ.media === "(pointer:coarse)") {
      hasTouchScreen = !!mQ.matches;
    } else if ("orientation" in window) {
      hasTouchScreen = true; // deprecated, but good fallback
    } else if (window.TouchEvent || "ontouchstart" in window) {
      result = true;
    } else {
      // Only as a last resort, fall back to user agent sniffing
      var UA = navigator.userAgent;
      hasTouchScreen =
        /\b(BlackBerry|webOS|iPhone|IEMobile)\b/i.test(UA) ||
        /\b(Android|Windows Phone|iPad|iPod)\b/i.test(UA);
    }
  }
  if (hasTouchScreen) {
    //console.log("TOUCHY!");
    //document.getElementById("touchy").innerText = "TOUCH ENABLED";

    document.getElementById("mList").innerHTML +=
      "<li>" + "Touchscreen: TRUE</li>";
  } else {
    document.getElementById("mList").innerHTML +=
      "<li>" + "Touchscreen: FALSE</li>";
  }
}

function checkPlugins() {
  plugins = [];
  //IE does not have .includes. Saving a few chars this way instead of switching to "indexOf(thing) >0
  try {
    plugins.includes = function (str) {
      var returnValue = false;
      if (this.indexOf(str) !== -1) {
        returnValue = true;
      }
      return returnValue;
    };
  } catch (e) {}
  if (window.navigator.plugins.length > 0) {
    bPlugins = navigator.plugins;
    Object.keys(bPlugins).forEach(function (item) {
      //console.log(item); // key
      document.getElementById("pList").innerHTML +=
        "<li>" + bPlugins[item].name + "</li>";
      plugins.push(bPlugins[item].name);
      console.log("Plug-in: " + bPlugins[item].name);
    });
    if (plugins.length == 1 && plugins.indexOf("Edge PDF Viewer") > -1) {
      document.getElementById("pList").innerHTML =
        "Very Likely stock Edge:<br>" +
        document.getElementById("pResults").innerHTML;
    } else if (plugins.indexOf("Edge PDF Viewer") > -1) {
      document.getElementById("pList").innerHTML =
        "Possibly Edge:<br>" + document.getElementById("pResults").innerHTML;
    } else if (
      plugins.length == 3 &&
      plugins.indexOf("Chrome PDF Plugin") > -1 &&
      plugins.indexOf("Chrome PDF Viewer") > -1 &&
      plugins.indexOf("Native Client") > -1
    ) {
      document.getElementById("pList").innerHTML =
        "Very Likely stock Chrome:<br>" +
        document.getElementById("pResults").innerHTML;
    } else if (
      plugins.indexOf("Chrome PDF Plugin") > -1 &&
      plugins.indexOf("Chrome PDF Viewer") > -1 &&
      plugins.indexOf("Native Client") > -1
    ) {
      document.getElementById("pList").innerHTML =
        "Possibly Chrome:<br>" + document.getElementById("pResults").innerHTML;
    } else if (
      plugins.length == 3 &&
      plugins.indexOf("Microsoft Edge PDF Plugin") > -1 &&
      plugins.indexOf("Microsoft Edge PDF Viewer") > -1 &&
      plugins.indexOf("Native Client") > -1
    ) {
      document.getElementById("pList").innerHTML =
        "Possibly stock Edge:<br>" +
        document.getElementById("pResults").innerHTML;
    } else if (
      plugins.indexOf("Microsoft Edge PDF Plugin") > -1 &&
      plugins.indexOf("Microsoft Edge PDF Viewer") > -1 &&
      plugins.indexOf("Native Client") > -1
    ) {
      document.getElementById("pList").innerHTML =
        "Possibly Edge:<br>" + document.getElementById("pResults").innerHTML;
    } else if (
      plugins.length == 4 &&
      plugins.indexOf("Chromium PDF Plugin") > -1 &&
      plugins.indexOf("Chromium PDF Viewer") > -1 &&
      plugins.indexOf("News feed handler") > -1 &&
      plugins.indexOf("Shockwave Flash") > -1
    ) {
      document.getElementById("pList").innerHTML =
        "Very Likely stock Opera:<br>" +
        document.getElementById("pResults").innerHTML;
    } else if (
      plugins.indexOf("Chromium PDF Plugin") > -1 &&
      plugins.indexOf("Chromium PDF Viewer") > -1 &&
      plugins.indexOf("News feed handler") > -1 &&
      plugins.indexOf("Shockwave Flash") > -1
    ) {
      document.getElementById("pList").innerHTML =
        "Possibly Opera:<br>" + document.getElementById("pResults").innerHTML;
    } else if (
      plugins.length >= 2 &&
      plugins.indexOf("Shockwave Flash") > -1 &&
      plugins.indexOf("Silverlight Plug-In") > -1
    ) {
      document.getElementById("pList").innerHTML =
        "Very Likely stock IE:<br>" +
        document.getElementById("pResults").innerHTML;
    } else if (
      plugins.indexOf("Shockwave Flash") > -1 &&
      plugins.indexOf("Silverlight Plug-In") > -1
    ) {
      document.getElementById("pList").innerHTML =
        "Possibly IE:<br>" + document.getElementById("pResults").innerHTML;
    } else if (plugins.length >= 1 && plugins[0] == "Shockwave Flash") {
      document.getElementById("pList").innerHTML =
        "Possibly Firefox:<br>" + document.getElementById("pResults").innerHTML;
    } else if (
      plugins.length == 2 &&
      plugins.indexOf("Chrome PDF Plugin") > -1 &&
      plugins.indexOf("Chrome PDF Viewer") > -1
    ) {
      document.getElementById("pList").innerHTML =
        "Possibly stock Brave:<br>" +
        document.getElementById("pResults").innerHTML;
    }
  } else {
    //console.log(bPlugins[item].name);
    document.getElementById("pList").innerHTML +=
      "<li>" +
      "No plug-ins: Possibly Safari mobile (rare), browser is blocking enumeration (Firefox or Tor), or removed for security (military)" +
      "</li>";
  }
}

function checkPlatform() {
  if (navigator.platform)
    //document.getElementById('platform').innerText = "Platform: " + navigator.platform;
    document.getElementById("mList").innerHTML +=
      "<li>" + "Platform: " + navigator.platform + "</li>";
}

function checkCss() {
  //css vendor prefix
  //https://davidwalsh.name/vendor-prefix
  //window.addEventListener("load", function () {
  var prefix = Array.prototype.slice
    .call(window.getComputedStyle(document.documentElement, ""))
    .join("")
    .match(/-(moz|webkit|ms)-/)[1];

  // moz - Firefox (Gecko Engine)
  // webkit - Chrome, Safari, Opera (Webkit Engine)
  // ms - Internet Explorer & Edge (Trident Engine)
  // NOTE - Old Opera versions use Presto Engine. Prefix is -o
  console.log("CSS prefix: " + prefix);
  document.getElementById("mList").innerHTML +=
    "<li>" + "CSS Vendor Check: " + prefix + "</li>";
  //});
}

function checkAgent() {
  var ua = navigator.userAgent,
    ver,
    M =
      ua.match(
        /(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i
      ) || [];
  if (/trident/i.test(M[1])) {
    ver = /\brv[ :]+(\d+)/g.exec(ua) || [];
    document.getElementById("uList").innerHTML =
      "IE" + " " + ver[1] + ": " + navigator.userAgent;
    return ["IE ", ver[1] || ""];
  }
  if (M[1] === "Chrome") {
    ver = ua.match(/\b(OPR|Edge|Edg)\/(\d+)/);
    if (ver != null) {
      ver = ver.slice(1);
      ver[0] = ver[0].replace("OPR", "Opera");
      ver[0] = ver[0].replace("Edg", "New Edge");
      document.getElementById("uList").innerHTML =
        ver[0] + " " + ver[1] + ": " + navigator.userAgent;
      return ver;
    }
  }
  M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, "-?"];
  if ((ver = ua.match(/version\/(\d+)/i)) != null) {
    M.splice(1, 1, ver[1]);
  }
  //return M;
  console.log("TEST");
  document.getElementById("uList").innerHTML =
    M[0] + " " + M[1] + ": " + navigator.userAgent;
}

function isPrivateMode() {
  return new Promise(function detect(resolve) {
    var yes = function () {
      resolve(true);
    }; // is in private mode
    var not = function () {
      resolve(false);
    }; // not in private mode

    function detectChromeOpera() {
      // https://developers.google.com/web/updates/2017/08/estimating-available-storage-space
      //var isChromeOpera = /(?=.*(opera|chrome)).*/i.test(navigator.userAgent) && navigator.storage && navigator.storage.estimate;
      var isChromeOpera = is["Chrome"] || is["Opera"];
      if (isChromeOpera) {
        navigator.storage.estimate().then(function (data) {
          return data.quota < 120000000 ? yes() : not();
        });
      }
      return !!isChromeOpera;
    }

    function detectFirefox() {
      //var isMozillaFirefox = 'MozAppearance' in document.documentElement.style;
      var isMozillaFirefox = is["Firefox"] || is["Tor"];
      if (isMozillaFirefox) {
        if (indexedDB == null) yes();
        else {
          var db = indexedDB.open("inPrivate");
          db.onsuccess = not;
          db.onerror = yes;
        }
      }
      return isMozillaFirefox;
    }

    function detectSafari() {
      //var isSafari = navigator.userAgent.match(/Version\/([0-9\._]+).*Safari/);
      var isSafari = is["Safari"];
      if (isSafari) {
        var testLocalStorage = function () {
          try {
            if (localStorage.length) not();
            else {
              localStorage.setItem("inPrivate", "0");
              localStorage.removeItem("inPrivate");
              not();
            }
          } catch (_) {
            // Safari only enables cookie in private mode
            // if cookie is disabled, then all client side storage is disabled
            // if all client side storage is disabled, then there is no point
            // in using private mode
            navigator.cookieEnabled ? yes() : not();
          }
          return true;
        };

        var version = parseInt(isSafari[1], 10);
        if (version < 11) return testLocalStorage();
        try {
          window.openDatabase(null, null, null, null);
          not();
        } catch (_) {
          yes();
        }
      }
      return !!isSafari;
    }

    function detectEdgeIE10() {
      //var isEdgeIE10 = !window.indexedDB && (window.PointerEvent || window.MSPointerEvent);
      var isEdgeIE10 = is["Edge"] || is["IE"] || is["New Edge"];
      if (isEdgeIE10) yes();
      return !!isEdgeIE10;
    }

    // when a browser is detected, it runs tests for that browser
    // and skips pointless testing for other browsers.
    if (detectChromeOpera()) return;
    if (detectFirefox()) return;
    if (detectSafari()) return;
    if (detectEdgeIE10()) return;

    // default navigation mode
    return not();
  });
}

// -------------------------------
function checkPrivate() {
  if (!is["IE"]) {
    setTimeout(function () {
      isPrivateMode().then(function (isPrivate) {
        console.log("Browsing in private mode? ", isPrivate);
        if (isPrivate) {
          document.getElementById("iList").innerHTML =
            "Browser is in Private/Incognito mode";
        }
      });
    }, 100);
  }
}

// function checkTemp() {
//   alert(TEMPORARY);
// }
//typeof window.ondevicemotion != "undefined"
function testers() {
  if (typeof ondeviceorientationabsolute != "undefined") {
    document.getElementById("mList").innerHTML +=
      "<li>" + "ondeviceorientationabsolute: YES</li>";
  } else {
    document.getElementById("mList").innerHTML +=
      "<li>" + "ondeviceorientationabsolute: no :(</li>";
  }
  if (typeof ondeviceorientation != "undefined") {
    document.getElementById("mList").innerHTML +=
      "<li>" + "ondeviceorientation: YES</li>";
  } else {
    document.getElementById("mList").innerHTML +=
      "<li>" + "ondeviceorientation: no :(</li>";
  }
  if (typeof ondevicemotion != "undefined") {
    document.getElementById("mList").innerHTML +=
      "<li>" + "ondevicemotion: YES</li>";
  } else {
    document.getElementById("mList").innerHTML +=
      "<li>" + "ondevicemotion: no :(</li>";
  }
}

//http://www.useragentstring.com/?uas=Opera/9.70%20(Linux%20i686%20;%20U;%20en-us)%20Presto/2.2.0&getText=all

// for (var prop in window)
//     console.log(prop);
